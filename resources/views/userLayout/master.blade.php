<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- favicon -->
    <link rel="icon" sizes="16x16" href="{{asset('assets/img/favicon.png')}}">

    <!-- Title -->
    <title> Давтлага цахим сургалт </title>

    <!-- Font Google -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- CSS Plugins -->
    <link rel="stylesheet" href="{{asset('assets/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/elegant-font-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
    <!-- main style -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">

</head>

<body>
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container-fluid">
            <!--logo-->
            <div class="logo">
                <a href="index.html">
                    <img src="assets/img/logo-dark.png" alt="" class="logo-dark">
                    <img src="assets/img/logo-white.png" alt="" class="logo-white">
                </a>
            </div>
            <!--/-->

            <!--navbar-collapse-->
            <div class="collapse navbar-collapse" id="main_nav">
                <ul class="navbar-nav ml-auto mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown" href="{{route('index.index')}}"> Нүүр хуудас </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{route('index.create')}}"> Хичээл </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link" href=""> Багш </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{url('blogIndex')}}"> Блог </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{url('aboutIndex')}}"> Бидний тухай </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{url('contactIndex')}}"> Холбоо Барих </a>
                    </li>
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link" href="login.html"> Нэвтрэх </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="register.html"> Бүртгүүлэх </a>
                    </li> -->

                    @if (Route::has('login'))
                    @auth
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{url('/teacher')}}"> Профайл </a>
                    </li>
                    @else
                    <li class="nav-item dropdown">
                        <!-- <a class="nav-link" href="register.html"> Бүртгүүлэх </a> -->
                        <a class="nav-link" href="{{ route('login') }}">Нэвтрэх</a>
                    </li>

                    @if (Route::has('register'))
                    <li class="nav-item dropdown">
                        <!-- <a class="nav-link" href="register.html"> Бүртгүүлэх </a> -->
                        <a class="nav-link" href="{{ route('register') }}">Бүртгүүлэх</a>
                    </li>
                    @endif
                    @endauth
                    @endif
                </ul>
            </div>
            <!--/-->

            <!--navbar-right-->
            <div class="navbar-right ml-auto">
                <div class="theme-switch-wrapper">
                    <label class="theme-switch" for="checkbox">
                        <input type="checkbox" id="checkbox" />
                        <div class="slider round"></div>
                    </label>
                </div>
                <div class="social-icones">
                    <ul class="list-inline">
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="#">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li> -->
                    </ul>
                </div>

                <div class="search-icon">
                    <i class="icon_search"></i>
                </div>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>


    @yield('index')

    @yield('lessonIndex')

    @yield('lessonSearchComponent')

    @yield('aboutIndex')

    @yield('blogIndex')

    @yield('contactIndex')

    @yield('errorIndex')

    @yield('blogPostMore')

    @yield('lessonBooking')

    <!--newslettre-->
    <section class="newslettre">
        <div class="container-fluid">
            <div class="newslettre-width text-center">
                <div class="newslettre-info">
                    <h5> Биднийг дэмжихийг хүсвэл доорх хуудсаар орж #Like #Share #Comment үлдээгээрэй </h5>
                    <!-- <p> Мөн #Mail хаягаа манай сайтанд бүртгүүлвэл цаг алдалгүй мэдээллийг авч байх болно. </p> -->
                </div>
                <!-- <form action="#" class="newslettre-form">
                    <div class="form-flex">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Таны цахим шуудангийн хаяг"
                                required="required">
                        </div>
                        <button class="submit-btn" type="submit">Бүртгүүлэх</button>
                    </div>
                </form> -->
                <div class="social-icones">
                    <ul class="list-inline">
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>Facebook</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-instagram"></i>Instagram </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-youtube"></i>Youtube</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--footer-->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-10 offset-sm-1 text-center">
                    <div class="copyright">
                        <p>&copy;
                            <script>
                            document.write(new Date().getFullYear());
                            </script>
                            <a href="#"> Эйс Ди Жи </a> Оюуны өмчөөр хамгаалав
                        </p>
                    </div>
                    <div class="back">
                        <a href="#" class="back-top">
                            <i class="arrow_up"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--Search-form-->
    <div class="search">
        <div class="container-fluid">
            <div class="search-width  text-center">
                <button type="button" class="close">
                    <i class="icon_close"></i>
                </button>
                <form class="search-form" action="#">
                    <input type="search" value="" placeholder="Хайхыг хүссэн зүйлээ бичнэ үү">
                    <button type="submit" class="search-btn">Хайх</button>
                </form>
            </div>
        </div>
    </div>
    <!--/-->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('assets/js/jquery-3.5.0.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

    <!-- JS Plugins  -->
    <script src="{{asset('assets/js/ajax-contact.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/switch.js')}}"></script>

    <!-- JS main  -->
    <script src="{{asset('assets/js/main.js')}}"></script>

    <!-- Lesson Range -->
    <script src="{{asset('assets/js/lesson-range.js')}}"></script>

    <script src="{{asset('assets/js/jquery-ui.js')}}"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("act");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
    </script>

</body>

</html>