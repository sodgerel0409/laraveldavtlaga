@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <a href="{{url('/adminProfile')}}"> Admin Profile </a>

                    <h6> Хэрэглэгч Тоо: {{$userCount}} </h6>
                    <h6> Багш Тоо: {{$teacherCount}} </h6>
                    <h6> Админ Тоо: {{$adminCount}} </h6>

                    @if(isset($teacherLesson))
                    @if(count($teacherLesson)>0)
                    @foreach($teacherLesson as $key=>$teacherLesson)
                    <ul>
                        <li>Хичээлийн Нэр: {{$teacherLesson->subClaName}}</li>
                        <li> Хичээлийн Танилцуулга: {{$teacherLesson->teaSelIntro}}</li>
                        <li> Багшийн Нэр: {{$teacherLesson->name}}</li>
                        <li> Хэддүгээр Ангид Орох: {{$teacherLesson->subClassNumber}}</li>

                    </ul>
                    @endforeach
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection