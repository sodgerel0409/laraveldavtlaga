@extends('userLayout.master')


@section('index')

<!-- slider start -->
<section class="section carousel-hero">
    <div class="owl-carousel">
        @foreach($slider as $slider)
        <div class="hero d-flex align-items-center"
            style="background-image: url('{{asset('assets/img/hero/1.jpg')}} ') ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                        <div class="hero-content">
                            <!-- <a href="blog-grid.html" class="categorie"> {{$slider->subClaName}} </a> -->
                            <h2>
                                <a href="post-default.html"> {{$slider->subClaName}}
                                </a>
                            </h2>
                            <div class="post-card-info">
                                <ul class="list-inline">
                                    <li>
                                        <a href="author.html">
                                            <img src="{{asset('assets/img/author/1.jpg')}}" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="author.html">{{$slider->name}}</a>
                                    </li>
                                    <li class="dot"></li>
                                    <li>{{$slider->created_at}}</li>
                                    <!-- <li class="dot"></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<!-- slider end -->

<!-- special lesson faq start -->
<section class="mt-80">
    <div class="container-fluid">
        <div class="upCatName">
            <h5> Онцлох Хичээл </h5>
            <div class="upCatNameBorder">

            </div>
        </div>
        <div class="row">
            @if(isset($specialLesson))
            @if(count($specialLesson)>0)
            @foreach($specialLesson as $key=> $specialLesson)
            <div class="col-lg-4 col-md-6">
                <!--Post-1-->
                <div class="post-card">
                    <div class="post-card-image">
                        <a href="post-default.html">
                            <img src="assets/img/hero/1.jpg" alt="">
                        </a>
                    </div>
                    <div class="post-card-content">
                        <a href="{{route('session.index', ['id' => $specialLesson->id, 'subClaId'=> $specialLesson->subClaId, 'teaSelSubClassId'=> $specialLesson->teaSelSubClassId, 'teaSelId'=> $specialLesson->teaSelId])}}"
                            class="categorie">
                            {{$specialLesson->subClaName}} </a>
                        <!-- <h5>
                            <a href="post-default.html"> Физик </a>
                        </h5> -->
                        <p> {{$specialLesson->teaSelIntro}}
                        </p>
                        <div class="post-card-info">
                            <ul class="list-inline">
                                <li>
                                    <a href="author.html">
                                        <img src="assets/img/author/1.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="author.html"> {{$specialLesson->name}} </a>
                                </li>
                                <li class="dot"></li>
                                <li>{{$specialLesson->created_at}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/-->
            </div>
            @endforeach
            @endif
            @endif
            <!--pagination-->
            <!-- <div class="col-lg-12">
                    <div class="pagination mt--10">
                        <ul class="list-inline">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="arrow_carrot-2right"></i></a></li>
                        </ul>
                    </div>
                </div> -->
        </div>
        <div class="upCatName">
            <h5> Шинээр Орох Боломжтой Хичээл </h5>
            <div class="upCatNameBorder">

            </div>
        </div>

        <div class="row">
            @if(isset($newLesson))
            @if(count($newLesson)>0)
            @foreach($newLesson as $key=> $newLesson)
            <div class="col-lg-4 col-md-6">
                <!--Post-1-->
                <div class="post-card">
                    <div class="post-card-image">
                        <a href="post-default.html">
                            <img src="{{asset('assets/img/hero')}}/{{$newLesson->subClaImage}}" alt="">
                        </a>
                    </div>
                    <div class="post-card-content">
                        <a href="blog-grid.html" class="categorie"> {{$newLesson->subClaName}} </a>
                        <p>

                        </p>
                        <div class="post-card-info">
                            <ul class="list-inline">
                                <li>
                                    <a href="author.html">
                                        <img src="assets/img/author/1.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="author.html"> Содгэрэл </a>
                                </li>
                                <li class="dot"></li>
                                <li>Гурван сарын 15, 2021</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/-->
            </div>
            @endforeach
            @endif
            @endif
        </div>
        <div class="upCatName">
            <h5> Түгээмэл Асуулт Хариулт </h5>
            <div class="upCatNameBorder">

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach($forever as $forever)
                <button class="accordion"> {{$forever->foreverAnswer}} </button>
                <div class="panel">
                    <p> {{$forever->foreverQuestion}} </p>
                </div>
                @endforeach
            </div>
        </div>

    </div>
</section>
<!-- special lesson faq end -->

@endsection
