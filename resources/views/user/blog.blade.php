@extends('../userLayout.master')

@section('blogIndex')

<!--category-->
<section class="categorie-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 alert alert-info">
                <div class="categorie-title">
                    <p>
                        <strong>
                            Энэхүү мэдээллийн хэсэг нь тухайн сайт дээр явагдаж байгаа хичээлийн талаар болон сурах
                            явцыг хөнгөвчлөх нэмэлт мэдээлэл оруулах зорилготой юм.
                        </strong>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!--blog-grid-->
<section class="blog-grid">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 mt-30">
                <div class="row">

                    <div class="col-lg-4 col-md-4">
                        <!--Post-1-->
                        <div class="post-card">
                            <div class="post-card-image">
                                <a href="post-default.html">
                                    <img src="assets/img/blog/1.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card-content">
                                <a href="blog-grid.html" class="categorie">Монгол Хэл</a>
                                <h5>
                                    <a href="postView.html"> Монгол Бичгийг Хэрхэн Хялбар Цээжлэх Вэ </a>
                                </h5>
                                <p> Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх
                                    Цээжлэх Цээжлэх Цээжлэх </p>
                                <div class="post-card-info">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="author.html">
                                                <img src="assets/img/author/1.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="author.html"> Содгэрэл </a>
                                        </li>
                                        <li class="dot"></li>
                                        <li> Нийтэлсэн Огноо </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/-->
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <!--Post-1-->
                        <div class="post-card">
                            <div class="post-card-image">
                                <a href="post-default.html">
                                    <img src="assets/img/blog/1.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card-content">
                                <a href="blog-grid.html" class="categorie">Монгол Хэл</a>
                                <h5>
                                    <a href="post-default.html"> Монгол Бичгийг Хэрхэн Хялбар Цээжлэх Вэ </a>
                                </h5>
                                <p> Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх
                                    Цээжлэх Цээжлэх Цээжлэх </p>
                                <div class="post-card-info">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="author.html">
                                                <img src="assets/img/author/1.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="author.html"> Содгэрэл </a>
                                        </li>
                                        <li class="dot"></li>
                                        <li> Нийтэлсэн Огноо </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/-->
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <!--Post-1-->
                        <div class="post-card">
                            <div class="post-card-image">
                                <a href="post-default.html">
                                    <img src="assets/img/blog/1.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-card-content">
                                <a href="blog-grid.html" class="categorie">Монгол Хэл</a>
                                <h5>
                                    <a href="post-default.html"> Монгол Бичгийг Хэрхэн Хялбар Цээжлэх Вэ </a>
                                </h5>
                                <p> Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх Цээжлэх
                                    Цээжлэх Цээжлэх Цээжлэх </p>
                                <div class="post-card-info">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="author.html">
                                                <img src="assets/img/author/1.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="author.html"> Содгэрэл </a>
                                        </li>
                                        <li class="dot"></li>
                                        <li> Нийтэлсэн Огноо </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/-->
                    </div>




                    <!--pagination-->
                    <div class="col-lg-12">
                        <div class="pagination mt--10">
                            <ul class="list-inline">
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li>
                                    <a href="#">2</a>
                                </li>
                                <li>
                                    <a href="#">3</a>
                                </li>
                                <li>
                                    <a href="#">4</a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="arrow_carrot-2right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/-->


@endsection