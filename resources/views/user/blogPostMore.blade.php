@extends('../userLayout.master')


@section('blogPostMore')

<section class="section pt-55 ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 mb-20">
                <!--Post-single-->
                <div class="post-single">
                    <div class="post-single-video">
                        <iframe width="1280" height="720" src="https://www.youtube.com/embed/Bq1Xl9yopFM"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                    <div class="post-single-content">
                        <h4> Мэдээний гарчиг </h4>
                        <div class="post-single-info">
                            <ul class="list-inline">
                                <li><a href="author.html"><img src="assets/img/author/1.jpg" alt=""></a></li>
                                <li><a href="author.html"> Нэр </a> </li>
                                <li class="dot"></li>
                                <li> Нийтэлсэн он </li>
                                <li class="dot"></li>
                                <li>3 сэтгэгдэл</li>
                            </ul>
                        </div>
                    </div>

                    <div class="post-single-body">
                        <p>
                            Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ
                            Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ
                            Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ
                            Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ
                            Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ
                            Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ
                            Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ Мэдээ
                        </p>
                    </div>

                    <div class="post-single-footer">
                        <div class="social-media">
                            <ul class="list-inline">
                                <li>
                                    <a href="#" class="color-facebook">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="color-instagram">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/-->

                <!--widget-comments-->
                <div class="widget mb-50">
                    <div class="title">
                        <h5>3 сэтгэгдэл</h5>
                    </div>
                    <ul class="widget-comments">
                        <li class="comment-item">
                            <img src="assets/img/author/1.jpg" alt="">
                            <div class="content">
                                <ul class="info list-inline">
                                    <li>Нэр</li>
                                    <li class="dot"></li>
                                    <li> Нийтэлсэн он</li>
                                </ul>

                                <p> сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл
                                    сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл
                                    сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл
                                    сэтгэгдэл сэтгэгдэл сэтгэгдэл сэтгэгдэл
                                </p>
                                <div>
                                    <a href="#" class="link">
                                        <i class="arrow_back"></i> Хариулах</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!--Leave-comments-->
                    <form class="widget-form" action="#" method="POST" id="main_contact_form">
                        <div class="alert alert-success contact_msg" style="display: none" role="alert">
                            Your message was sent successfully.
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="message" id="message" cols="30" rows="5" class="form-control"
                                        placeholder="Message*" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name*"
                                        required="required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control"
                                        placeholder="Email*" required="required">
                                </div>
                            </div>
                            <div class="col-12 mb-20">
                                <div class="form-group">
                                    <input type="text" name="website" id="website" class="form-control"
                                        placeholder="website">
                                </div>
                                <!-- <label>
                                        <input name="name" type="checkbox" value="1" required="required">
                                        <span>  </span>
                                    </label> -->
                            </div>
                            <div class="col-12">
                                <button type="submit" name="submit" class="btn-custom">
                                    Нийтлэх
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 max-width">
                <!--widget-author-->
                <div class="widget">
                    <div class="widget-author">
                        <a href="author.html" class="image">
                            <img src="assets/img/author/1.jpg" alt="">
                        </a>
                        <h6>
                            <span> Сайн байна уу намайг тэр гэдэг </span>
                        </h6>
                        <p>
                            Сайн байна уу намайг тэр гэдэг би тэр ийм чиглэлээр тэр сургуульд суралцаж төгсөөд тэр
                            сургуульд багшилдаг тэдэн жилийн туршлагатай
                        </p>
                        <div class="social-media">
                            <ul class="list-inline">
                                <li>
                                    <a href="#" class="color-facebook">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="color-instagram">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="color-youtube">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="color-pinterest">
                                        <i class="fas fa-envelope"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/-->

                <!--widget-latest-posts-->
                <div class="widget ">
                    <div class="section-title">
                        <h5> Тухайн Хүний Сүүлийн Үеийн Нийтлэл </h5>
                    </div>
                    <ul class="widget-latest-posts">
                        <li class="last-post">
                            <div class="image">
                                <a href="post-default.html">
                                    <img src="assets/img/latest/1.jpg" alt="...">
                                </a>
                            </div>
                            <div class="nb">1</div>
                            <div class="content">
                                <p><a href="post-default.html"> Мэдээний Гарчиг </a></p>
                                <small><span class="icon_clock_alt"></span> Нийтэлсэн он</small>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--/-->

                <!--widget-instagram-->
                <div class="widget">
                    <div class="section-title">
                        <h5>Instagram</h5>
                    </div>
                    <ul class="widget-instagram">
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/1.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/2.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/3.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/4.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/5.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/6.jpg" alt="">
                            </a>
                        </li>
                    </ul>

                </div>
                <!--/-->
            </div>
        </div>
    </div>
</section>

@endsection