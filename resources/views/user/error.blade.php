@extends('../userLayout.master')

@section('errorIndex')

<section class="section pt-55 mb-50">
    <div class="container-fluid">
        <div class="page404  widget">
            <div class="image">
                <img src="assets/img/404.jpg" alt="">
            </div>
            <div class="content">
                <h1>404</h1>
                <h3> Ийм Хуудас Олдсонгүй </h3>
                <a href="index.html" class="btn-custom"> Нүүр Хуудасруу Буцах </a>
            </div>
        </div>
    </div>
</section>

@endsection