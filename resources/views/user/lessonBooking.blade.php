@extends('../userLayout.master')

@section('lessonBooking')

<section class="section author full-space mb-40 pt-55">
    <div class="container-fluid">
        <div class="row">
            @if(isset($teacherView))
            @if(count($teacherView)>0)
            @foreach($teacherView as $key=> $teacherView)
            <div class="col-lg-12">
                <!--widget-author-->
                <div class="widget-author ">
                    <a href="author.html" class="image">
                        <img src="assets/img/author/1.jpg" alt="">
                    </a>
                    <h6><span> {{$teacherView->name}}</span></h6>
                    <div class="link"> {{$teacherView->subClaName}}
                        {{$teacherView->subClassNumber}} </div>

                    <p> {{$teacherView->introduction}}
                    </p>

                    <div class="social-media">
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="color-facebook">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="color-instagram">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="color-youtube">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div style="margin-top: 20px;" class="col-lg-12">
                <div class="widget-author">
                    {{$teacherView->teaSelIntro}}
                </div>
            </div>
            @endforeach
            @endif
            @endif
        </div>
    </div>
</section>

<!--mansory-layout-->
<section class="masonry-layout col2-layout mt-30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @if(session()->has('message'))
                <div style="text-align: center;" class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
                @endif
                <form action="{{route('session.store')}}" enctype="multipart/form-data" method="post">
                    @csrf
                    @if(isset($teacherTime))
                    @if(count($teacherTime)>0)
                    @foreach($teacherTime as $key=> $teacherTime)
                    <div style="background-color:burlywood; float: left; margin:10px; padding:5px;">
                        <input class="@error('teaTime') is-invalid @enderror" type="checkbox" name="sStartTime" id=""
                            value="{{$teacherTime->teaTimeSelTime}}">
                        <label for="">{{$teacherTime->teaTimeSelTime}} </label>
                        @error('sStartTime')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    @endforeach
                    @endif
                    @endif
                    <input type="hidden" name="sTeaSelSubClassId" value="{{$teacherView->subClassId}}">
                    <input type="hidden" name="sTeaSelSubClaId" value="{{$teacherView->subClaId}}">
                    <input type="hidden" name="sStudentId" value="{{auth()->user()->id}}">
                    <input type="hidden" name="sTeacherSelectId" value="{{$teacherView->id}}">
                    <?php date_default_timezone_set("Asia/Ulaanbaatar");?>
                    <input type="hidden" name="sBookingDate"
                        value="<?php echo date_default_timezone_get() . " " . date("Y/m/d/H:i:s") ?>" id="">

                    <input type="date" name="sStartDate" value="<?php echo date("Y-m-d") ?>"
                        min="<?php echo date("Y-m-d") ?>">
                    <button type="submit"> Захиалах </button>
                </form>
            </div>
        </div>
    </div>
</section>


@endsection