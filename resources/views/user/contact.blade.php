@extends('../userLayout.master')

@section('contactIndex')

<!--contact us-->
<section class="section pt-50">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h5>Contact us</h5>
                </div>
            </div>
        </div>

        <div class="row mb-20">
            <div class="col-lg-8 mt-30">
                <div class="contact">
                    <div class="google-map">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1173.1094010624968!2d106.92213010165297!3d47.92123519477598!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5d9692410e9a41b5%3A0x3372eb2a5a6155bf!2sInformation%20Technology%20Park!5e1!3m2!1sen!2smn!4v1636364422268!5m2!1sen!2smn"
                            width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </iframe>
                    </div>
                    <form action="assets/php/mail.php" class="widget-form contact_form " method="POST"
                        id="main_contact_form">
                        <p class="alert alert-info"> Бидэнтэй хамтран ажиллах хүсэлтэй сургалтын төв болон багш
                            доорх хэсэгт мэдээллээ оруулан холбогдох боломжтой </p>
                        <!-- <div class="alert alert-success contact_msg" style="display: none" role="alert">
                                Your message was sent successfully.
                            </div> -->
                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Your Name*"
                                required="required">
                        </div>

                        <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Your Email*"
                                required="required">
                        </div>

                        <div class="form-group">
                            <input type="text" name="subject" id="subject" class="form-control"
                                placeholder="Your Subject*" required="required">
                        </div>

                        <div class="form-group">
                            <textarea name="message" id="message" cols="30" rows="5" class="form-control"
                                placeholder="Your Message*" required="required"></textarea>
                        </div>

                        <button type="submit" name="submit" class="btn-custom">Send Message</button>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 max-width">
                <!--widget-latest-posts-->
                <div class="widget ">
                    <div class="section-title">
                        <h5> Сүүлийн үеийн пост </h5>
                    </div>
                    <ul class="widget-latest-posts">
                        <li class="last-post">
                            <div class="image">
                                <a href="post-default.html">
                                    <img src="assets/img/latest/1.jpg" alt="...">
                                </a>
                            </div>
                            <div class="nb">1</div>
                            <div class="content">
                                <p>
                                    <a href="post-default.html"> Физикийн тухай сонирхолтой онол </a>
                                </p>
                                <small>
                                    <span class="icon_clock_alt"></span> Нийтэлсэн он </small>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--/-->

                <!--widget-instagram-->
                <div class="widget">
                    <div class="section-title">
                        <h5> Зураг </h5>
                    </div>
                    <ul class="widget-instagram">
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/1.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/2.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/3.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/4.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/5.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a class="image" href="#">
                                <img src="assets/img/instagram/6.jpg" alt="">
                            </a>
                        </li>
                    </ul>

                </div>
                <!--/-->
            </div>
        </div>

    </div>
</section>

@endsection