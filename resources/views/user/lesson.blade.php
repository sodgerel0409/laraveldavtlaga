@extends('../userLayout.master')


@section('lessonIndex')
<!--post-default-->
<section class="section pt-55">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 max-width">
                <!-- Lesson Check -->
                <div class="widget">
                    <div class="widget-author">
                        <h6>
                            <span> Хичээл Сонголт </span>
                        </h6>
                    </div>
                    @php
                    $i=1;
                    @endphp
                    @if(isset($subClaName))
                    @if(count($subClaName)>0)
                    @foreach($subClaName as $key => $value)
                    <div class="form-check">
                        <label class="form-check-label" for="">

                            <a class="link" style="margin: 5px; font-size:medium;"
                                href="{{route('index.create', ['lessonId'=> $value->subClaId])}}">
                                {{$i++}}) {{$value->subClaName}}
                            </a>
                        </label>
                    </div>


                    @endforeach
                    @endif
                    @endif
                </div>
                <!-- Class Check -->
                <div class=" widget">
                    <div class="widget-author">
                        <h6>
                            <span> Анги Сонголт </span>
                        </h6>
                    </div>
                    @if(isset($classNumber))
                    @if(count($classNumber)>0)
                    @foreach($classNumber as $key=> $sNumber)
                    <div class="form-check">
                        <label class="form-check-label" for="">

                            <a class="link" style="margin: 5px; font-size:medium;"
                                href="{{route('index.create', ['sNumber'=> $sNumber->subClassId])}}">
                                {{$sNumber->subClassNumber}} - р анги
                            </a>
                        </label>
                    </div>
                    @endforeach
                    @endif
                    @endif

                </div>
                <!--/-->
                <!-- <div class="widget">
                    <div class="widget-author">
                        <h6>
                            <span> Үнийн Дүн Заах </span>
                        </h6>
                    </div>
                    <strong class="range-slider__value">

                        <p> Үнийн Дүн: </p>
                    </strong>
                    <div class="range-slider__slider">
                        <input type="range" min="1000" max="30000" step="1000" value="1000" class="range"
                            id="rangeSlider" />
                    </div>
                </div> -->
            </div>

            @section('lessonSearchComponent')
            <div class="col-lg-8 mb-20">
                @if(isset($teacherSelectionLesson))
                @if(count($teacherSelectionLesson)>0)
                @foreach($teacherSelectionLesson as $key => $teaLesson)
                <div class="widget mb-50 filter_data">
                    <ul class="widget-comments">
                        <li class="comment-item">
                            <img src="{{asset('assets/img/author/1.jpg')}}" alt="">
                            <div class="content">
                                <ul class="info list-inline">
                                    <li> {{$teaLesson->name}} </li>
                                    <li class="dot"></li>
                                    <li> Элссэн Он </li>
                                    <li class="dot"></li>
                                    <li> Ажиллсан Жил </li>
                                </ul>
                                <p> {{$teaLesson->teaSelIntro}}
                                </p>
                                <div>
                                    <a class="link">
                                        </i> {{$teaLesson->subClaName}} </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                @endforeach

                @else
                <section class="section pt-55 mb-50">
                    <div class="container-fluid">
                        <div class="page404  widget">
                            <div class="image">
                                <img src="assets/img/404.jpg" alt="">
                            </div>
                            <div class="content">
                                <h1>404</h1>
                                <h3> Илэрц Байхгүй Байна </h3>
                            </div>
                        </div>
                    </div>
                </section>
                @endif
                @endif
                <div class="col-lg-12">
                    <div class="pagination mt--10">
                        <ul class="list-inline">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="arrow_carrot-2right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





@endsection



<!-- <script type="text/javascript">
$(document).ready(function() {
    console.log("test");

    $(function filter_data() {
        var action = "{{route('index.create')}}";
        var subName = get_filter('subName');
        var subNumber = get_filter('subNumber');
        console.log(subName);

        $.ajax({
            url: "{{route('index.create')}}",
            method: "get",
            data: {
                action: action,
                subName: subName,
                subNumber: subNumber
            },
            success: function(data) {
                $('.filter_data').html(data);
                console.log(filter_data);
            }
        });
    });

    function get_filter() {
        var filter = [];
        $('.' + subName + ':checked').each(function() {
            filter.push($(this).val());
            console.log("test");
        });
        return filter;
    }
});
</script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

@endsection
