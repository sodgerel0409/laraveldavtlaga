@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <section>
                        <div>
                            <h5>
                                <span> Хэрэглэгчийн Нэр: </span> {{auth()->user()->name}}
                            </h5>
                            <h5>
                                <span> Хэрэглэгчийн Цахим Хаяг: </span> {{auth()->user()->email}}
                            </h5>
                            <h5>
                                <span> Хэрэглэгчийн Овог Нэр: </span> {{auth()->user()->lastname}}
                            </h5>
                            <h5>
                                <span> Хэрэглэгчийн зураг: </span> {{auth()->user()->lastname}}
                            </h5>
                            <h5>
                                <span> Хүйс: </span> {{auth()->user()->gender}}
                            </h5>
                            <h5>
                                <span> Төрсөн Он Сар Өдөр: </span> {{auth()->user()->birthday}}
                            </h5>
                            <h5>
                                <span> Регистерийн Дугаар: </span> {{auth()->user()->registerNumber}}
                            </h5>
                            <h5>
                                <span> Гар Утасны Дугаар: </span> {{auth()->user()->phone}}
                            </h5>
                            <h5>
                                <span> Суралцаж Байгаа Сургууль: </span> {{auth()->user()->school}}
                            </h5>
                            <h5>
                                <span> Суралцаж Буй Анги: </span> {{auth()->user()->class}}
                            </h5>
                            <h5>
                                <span> Хэрэглэгчийн Танилцуулга </span> {{auth()->user()->introduction}}
                            </h5>
                            <h5>
                                <span> Аав Нэр </span> {{auth()->user()->fatherName}}
                            </h5>
                            <h5>
                                <span> Аав/дугаар </span> {{auth()->user()->fatherPhone}}
                            </h5>
                            <h5>
                                <span> Ээж Нэр: </span> {{auth()->user()->motherName}}
                            </h5>
                            <h5>
                                <span> Ээж/дугаар: </span> {{auth()->user()->motherPhone}}
                            </h5>
                            <h5>
                                <span> Гэрийн хаяг: </span> {{auth()->user()->Address}}
                            </h5>
                        </div>
                        <div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
