<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('lastName')->nullable();
            $table->string('image')->nullable();
            $table->string('gender')->nullable();
            $table->date('birthDay')->nullable();
            $table->string('registerNumber')->nullable()->unique();
            $table->integer('phone')->nullable()->unique();
            $table->string('school')->nullable();
            $table->string('class')->nullable();
            $table->string('cerficateNumber')->unique()->nullable();
            $table->text('introduction')->nullable();
            $table->string('fatherName')->nullable();
            $table->integer('fatherPhone')->nullable();
            $table->string('motherName')->nullable();
            $table->integer('motherPhone')->nullable();
            $table->text('addresss')->nullable();
            $table->integer('role')->nullable();
            $table->integer('status_id')->nullable();
            $table->boolean('softDelete')->nullable();
            $table->rememberToken();
            $table->timestamps();

            /* userId,
        userLastName,
        userFirstName,
        userGender,
        userBirthDay,
        userRegNumber,
        userPhone,
        userEmail,
        userSchool,
        userClass,
        userCerficateNumber,
        userIntro,
        userFatherName,
        userFatherPhone,
        userMotherName,
        userMotherPhone,
        userAddress */

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}