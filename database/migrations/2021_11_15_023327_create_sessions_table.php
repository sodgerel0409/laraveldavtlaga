<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->id('sessionId');
            //subject class id
            $table->integer('sTeaSelSubClaId');
            //sub class id
            $table->integer('sTeaSelSubClassId');
            //teacher selection Id avah
            $table->string('sBookingDate');
            $table->integer('sTeacherSelectId');
            //hicheel zahialah ueiin ognoo date
            $table->integer('sStudentId');
            //davtlaga ehleh estoi tsagiin ognoo and date
            $table->date('sStartDate');
            $table->time('sStartTime');
            //davtlaga ehelsen ognoo
            $table->dateTime('sStartDateTimeReal');
            //davtlaga duusah estoi ognoo and Time
            $table->dateTime('sEndDateTime');
            //davtlaga duussan ognoo
            $table->dateTime('sEndDateTimeReal');
            //herev tsutsalsan bol 0/1
            $table->integer('sisCancelled');
            //tsutsalsan bagsh bolon suragchiin id
            $table->integer('sCancelledBy');
            //tsutsalsan ognoo and time
            $table->date('sCancelledDateTime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
