<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherTimerSelectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_timer_selections', function (Blueprint $table) {
            $table->id('teaTimeSelId');
            $table->time('teaTimeSelTime');
            $table->integer('teaTimeSelTeacherId');
            $table->boolean('teaTimeSelBoolean')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_timer_selections');
    }
}