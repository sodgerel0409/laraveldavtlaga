<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherSelectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_selections', function (Blueprint $table) {
            $table->id('teaSelId');
            $table->integer('teaSelSubClaId'); //Hicheeliin ner
            $table->string('teaSelIntro'); //hicheeliin taniltsuulga
            $table->integer('teaSelTeaId'); //bagshiin id
            $table->integer('teaSelSubClassId'); // ymar angid oroh
            $table->boolean('teaSelSoftDelete')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_selections');
    }
}