<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        //Role 1 = admin
        if (Auth::user()->role == 1) {
            return redirect()->route('admin');
        }
        //Role 2 = teacher
        if (Auth::user()->role == 2) {
            return redirect()->route('teacher');
        }
        //Role 3 = user
        if (Auth::user()->role == 3) {
            return $next($request);
        }
    }
}