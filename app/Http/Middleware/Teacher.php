<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Teacher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        //Role 1 = admin
        elseif (Auth::user()->role == 1) {
            return redirect()->route('admin');
        }
        //Role 2 = teacher
        elseif (Auth::user()->role == 2) {
            return $next($request);
        }
        //Role 3 = user
        elseif (Auth::user()->role == 3) {
            return redirect()->route('user');
        }
    }
}