<?php

namespace App\Http\Controllers;

use App\Model\SubClass;
use Illuminate\Http\Request;

class SubClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\SubClass  $subClass
     * @return \Illuminate\Http\Response
     */
    public function show(SubClass $subClass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\SubClass  $subClass
     * @return \Illuminate\Http\Response
     */
    public function edit(SubClass $subClass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\SubClass  $subClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubClass $subClass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\SubClass  $subClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubClass $subClass)
    {
        //
    }
}
