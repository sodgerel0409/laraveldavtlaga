<?php

namespace App\Http\Controllers;

use App\Model\TeacherSelection;
use Illuminate\Http\Request;

class TeacherSelectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\TeacherSelection  $teacherSelection
     * @return \Illuminate\Http\Response
     */
    public function show(TeacherSelection $teacherSelection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\TeacherSelection  $teacherSelection
     * @return \Illuminate\Http\Response
     */
    public function edit(TeacherSelection $teacherSelection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\TeacherSelection  $teacherSelection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeacherSelection $teacherSelection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\TeacherSelection  $teacherSelection
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeacherSelection $teacherSelection)
    {
        //
    }
}
