<?php

namespace App\Http\Controllers;

use App\Model\TeacherTimerSelection;
use Illuminate\Http\Request;

class TeacherTimerSelectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\TeacherTimerSelection  $teacherTimerSelection
     * @return \Illuminate\Http\Response
     */
    public function show(TeacherTimerSelection $teacherTimerSelection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\TeacherTimerSelection  $teacherTimerSelection
     * @return \Illuminate\Http\Response
     */
    public function edit(TeacherTimerSelection $teacherTimerSelection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\TeacherTimerSelection  $teacherTimerSelection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeacherTimerSelection $teacherTimerSelection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\TeacherTimerSelection  $teacherTimerSelection
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeacherTimerSelection $teacherTimerSelection)
    {
        //
    }
}
