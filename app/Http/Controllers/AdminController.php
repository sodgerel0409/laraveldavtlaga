<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\About  $about
     * @return \Illuminate\Http\Response
     */
    public function show(About $about)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit(About $about)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, About $about)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        //
    }

    public function adminRole()
    {
        $userCount = User::where('role', '=', '3')->count();
        $teacherCount = User::where('role', '=', '2')->count();
        $adminCount = User::where('role', '=', '1')->count();

        $teacherLesson = DB::select('SELECT subject_classes.subClaName, teacher_selections.teaSelIntro, users.name, sub_classes.subClassNumber FROM sub_classes, subject_classes, teacher_selections, users WHERE subject_classes.subClaId = teacher_selections.teaSelSubClaId AND teacher_selections.teaSelTeaId=users.id AND teacher_selections.teaSelSubClassId = sub_classes.subClassId ORDER BY teacher_selections.teaSelId DESC');
        return view('admin.index', ['userCount' => $userCount, 'teacherCount' => $teacherCount, 'adminCount' => $adminCount, 'teacherLesson' => $teacherLesson]);

    }
    public function adminProfile()
    {
        return view('admin.profile');
    }
}