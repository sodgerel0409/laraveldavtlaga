<?php

namespace App\Http\Controllers;

use App\Model\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $rules = [
        'sStartTime' => 'required',
    ];
    public function index()
    {
        $userId = $_GET['id'];
        $subClaId = $_GET['subClaId'];
        $teaSelSubClassId = $_GET['teaSelSubClassId'];
        $teaSelId = $_GET['teaSelId'];
        $teacherView = DB::select('SELECT subject_classes.subClaId, sub_classes.subClassId, users.id, users.name, users.introduction, users.image, subject_classes.subClaName, sub_classes.subClassNumber, teacher_selections.teaSelIntro FROM users, subject_classes, sub_classes, teacher_selections WHERE users.id = ' . $userId . ' AND subject_classes.subClaId = ' . $subClaId . ' AND sub_classes.subClassId = ' . $teaSelSubClassId . ' AND teacher_selections.teaSelId = ' . $teaSelId . ' ');

        $teacherTime = DB::select('SELECT teacher_timer_selections.teaTimeSelTime FROM teacher_timer_selections WHERE teacher_timer_selections.teaTimeSelTeacherId = ' . $userId . ' ORDER BY teacher_timer_selections.teaTimeSelTime ASC');
        // dd($teacherView);
        return view('user.lessonBooking', ['teacherView' => $teacherView, 'teacherTime' => $teacherTime]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $newSession = new Session;
        $newSession->sBookingDate = $request->get('sBookingDate');
        $newSession->sTeacherSelectId = $request->get('sTeacherSelectId');
        $newSession->sStudentId = $request->get('sStudentId');
        $newSession->sStartTime = $request->get('sStartTime');
        $newSession->sStartDate = $request->get('sStartDate');
        $newSession->sTeaSelSubClassId = $request->get('sTeaSelSubClassId');
        $newSession->sTeaSelSubClaId = $request->get('sTeaSelSubClaId');
        $newSession->save();
        return back()->with('message', 'Давтлага Хичээлийн Захиалага Амжилттай Хийгдлээ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Session $session)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function destroy(Session $session)
    {
        //
    }
}