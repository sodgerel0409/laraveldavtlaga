<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specialLesson = DB::select('SELECT teacher_selections.teaSelId, teacher_selections.created_at, users.name, teacher_selections.teaSelIntro, subject_classes.subClaName, subject_classes.subClaImage, users.id, subject_classes.subClaId, teacher_selections.teaSelSubClassId FROM sub_classes, users, teacher_selections, subject_classes WHERE teacher_selections.teaSelSubClaId=subject_classes.subClaId AND teacher_selections.teaSelTeaId=users.id AND teacher_selections.teaSelSubClassId=sub_classes.subClassId  ORDER BY teacher_selections.teaSelId ASC LIMIT 4');

        $slider = DB::select('SELECT subject_classes.created_at ,subject_classes.subClaImage, users.name, subject_classes.subClaName, subject_classes.subClaImage FROM users, teacher_selections, subject_classes WHERE teacher_selections.teaSelSubClaId=subject_classes.subClaId AND teacher_selections.teaSelTeaId=users.id ORDER BY subject_classes.subClaId ASC LIMIT 9');

        $forever = DB::select('SELECT forever_a_q_s.foreverAnswer, forever_a_q_s.foreverQuestion FROM forever_a_q_s');

        $newLesson = DB::select('SELECT subject_classes.subClaName, subject_classes.subClaImage FROM subject_classes WHERE subject_classes.subClaCommingSoon=1 ORDER BY subject_classes.subClaCommingSoon ASC LIMIT 9');

        return view("welcome", ['specialLesson' => $specialLesson, 'newLesson' => $newLesson, 'forever' => $forever, 'slider' => $slider]);

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd($lessonId = $_GET['lessonId']);
        $subClaName = DB::select('SELECT subject_classes.subClaId, subject_classes.subClaName FROM subject_classes');
        $classNumber = DB::select('SELECT sub_classes.subClassId, sub_classes.subClassNumber FROM sub_classes');

        if (isset($_GET['lessonId'])) {
            $lessonId = $_GET['lessonId'];
            $teacherSelectionLesson = DB::select("SELECT teacher_selections.teaSelIntro, users.name, subject_classes.subClaName FROM teacher_selections, users, subject_classes WHERE teacher_selections.teaSelTeaId=users.id AND teacher_selections.teaSelSubClaId=subject_classes.subClaId AND teacher_selections.teaSelSubClaId = $lessonId ");
            return view('user/lesson', ['subClaName' => $subClaName, 'classNumber' => $classNumber, 'teacherSelectionLesson' => $teacherSelectionLesson]);
        } elseif (isset($_GET['sNumber'])) {
            $sNumber = $_GET['sNumber'];
            $teacherSelectionLesson = DB::select("SELECT teacher_selections.teaSelIntro, users.name, subject_classes.subClaName FROM teacher_selections, users, subject_classes WHERE teacher_selections.teaSelTeaId=users.id AND teacher_selections.teaSelSubClaId=subject_classes.subClaId AND teacher_selections.teaSelSubClassId = $sNumber ");
            return view('user/lesson', ['subClaName' => $subClaName, 'classNumber' => $classNumber, 'teacherSelectionLesson' => $teacherSelectionLesson]);
        }
        return view('user/lesson', ['subClaName' => $subClaName, 'classNumber' => $classNumber]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lessonIndex()
    {

    }

    public function aboutIndex()
    {
        return view('user/about');
    }

    public function blogIndex()
    {
        return view('user.blog');
    }

    public function contactIndex()
    {
        return view('user.contact');
    }

    public function errorIndex()
    {
        return view('user.error');
    }

    public function blogPostMore()
    {
        return view('user.blogPostMore');
    }

    public function lessonBooking()
    {
        return view('user.lessonBooking');
    }
}