<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TeacherSelection extends Model
{
    protected $table = "teacher_selections";
    protected $primarykey = "teaSelId";
    protected $fillable = ['teaSelSubClaId', 'teaSelIntro', 'teaSelTeaId', 'teaSeaSoftDelete'];
    public $timestamps = true;
}