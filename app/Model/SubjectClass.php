<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubjectClass extends Model
{
    protected $table = "subject_classes";
    protected $primarykey = "subClaId";
    protected $fillable = ['subClaName', 'subClaOne', 'subClaTwo', 'subClaThree', 'subClaFour', 'subClaFive', 'subClaSix', 'subClaSeven', 'subClaEight', 'subClaNine', 'subClaTen', 'subClaEleven', 'subClaTwele'];
    public $timestamps = true;
}