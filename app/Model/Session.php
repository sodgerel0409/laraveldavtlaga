<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table = "sessions";
    protected $primarykey = "sessionId";
    protected $fillable = ['sBookingDate', 'sBookingTime', 'sTeacherSelectId', 'sStudentId', 'sStartDate', 'sStartTime', 'sStartDateReal', 'sStartTimeReal', 'sEndDate', 'sEndTime', 'sEndDateReal', 'sEndTimeReal', 'sisCancelled', 'sCancelledBy', 'sCancelledDate', 'sCancelledTime'];
    public $timestamps = true;
}