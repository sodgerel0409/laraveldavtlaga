<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect()->to('index');
});
Route::get('lessonIndex', 'MainController@lessonIndex');
Route::get('aboutIndex', 'MainController@aboutIndex');
Route::get('blogIndex', 'MainController@blogIndex');
Route::get('contactIndex', 'MainController@contactIndex');
Route::get('errorIndex', 'MainController@errorIndex');
Route::get('blogPostMore', 'MainController@blogPostMore');
Route::get('lessonBooking', 'MainController@lessonBooking');

Route::resource('index', 'MainController');
Route::resource('session', 'SessionController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('user')->middleware('user');

Route::get('/admin', 'AdminController@adminRole')->name('admin')->middleware('admin');
Route::get('adminProfile', 'AdminController@adminProfile');
Route::resource('adminc', 'AdminController');

// Route::get('/user', 'UserController@index')->name('user')->middleware('user');
Route::get('/teacher', 'TeacherController@index')->name('teacher')->middleware('teacher');